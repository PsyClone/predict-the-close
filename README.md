# Predict the Close

A prediction model built in Python to estimate the closing price and volume of stocks based on historic data.

Created as part of APAC Hackathon 2018 organised by Credit Suisse (team **fsociety**).

The dataset is available [here](https://drive.google.com/drive/folders/1k743itLNnNY5O6POGV-zEIU7OaaBTh7j).